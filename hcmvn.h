#ifndef HCMVN_H
#define HCMVN_H
#include "Eigen/Dense"
double hccmvn(Eigen::MatrixXd covM, Eigen::VectorXd a, Eigen::VectorXd b,
        int m, int d, double tol = 1e-6);
#endif // HCMVN_H
