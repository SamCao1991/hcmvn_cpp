#include "conditioning.h"
#include "qmc.h"
#include "mkl.h"
#include "commondcl.h"
#include <iostream>

using namespace std;
using namespace Eigen;

const int SAMPLE_NUM = 10;

#define ALMOSTONE 0.9999999999
#define ALMOSTZERO 0.0000000001

void kdConditioning(const Eigen::MatrixXd &L,
    const Eigen::VectorXd &a, const Eigen::VectorXd &b, int d, double &p, Eigen::VectorXd &y)
{
    int N = L.rows();
    p = 1.0;
    y.resize(N);
    y.array() = 0;
    int RAND_POINT_NUM,SAMPLE_NUM;
    if(d <= 4)
    {
        RAND_POINT_NUM = 2e2;
        SAMPLE_NUM = 5;
    }

    if(d <= 8 && d > 4)
    {
        RAND_POINT_NUM = 4e2;
        SAMPLE_NUM = 10;
    }
    if(d > 8)
    {
        RAND_POINT_NUM = 1e3;
        SAMPLE_NUM = 10;
    }
	
    // Main loop for blocks of size d
    // Use curly bracket to limit the scope of the variables
    {
        double psub,esub,psubsubLower,psubsubUpper,esubsub;
        VectorXd g(d),normalizedasubsub(d-1),normalizedbsubsub(d-1),normalizedAsubsubCol(d-1);
        MatrixXd normalizedLsub(d,d), normalizedLsubsub(d-1,d-1);
        ArrayXd stdevsub(d);
        for(int i = 0; i < N - d + 1; i = i + d)
        {
            // Calculation prob
            g = VectorXd::Zero(d);
            for(int j = 0; j < i; j = j + d)
                g += L.block(i,j,d,d) * L.block(j,j,d,d).inverse() * y.segment(j,d);
            normalizedLsub = L.block(i,i,d,d);
            normalizedLsub.triangularView<StrictlyUpper>().setZero();
            MatrixXd normalizedAsub = normalizedLsub * normalizedLsub.transpose();
            stdevsub = normalizedAsub.diagonal().array().sqrt();
            stdevsub = stdevsub.unaryExpr([](double x){return x < ALMOSTZERO? ALMOSTZERO : x; });
            VectorXd normalizedasub = ((a.segment(i,d) - g).array() / stdevsub).matrix();
            VectorXd normalizedbsub = ((b.segment(i,d) - g).array() / stdevsub).matrix();
            for(int j = 0; j < d; j++)
                for(int k = 0; k < d; k++)
                {
                    normalizedLsub(j,k) /= stdevsub(j);
                    normalizedAsub(j,k) /= (stdevsub(j) * stdevsub(k));
                }
            mvn(RAND_POINT_NUM,normalizedLsub,normalizedasub,normalizedbsub,psub,esub,SAMPLE_NUM);
            p = p * psub;

            // Calculate expectation
            VectorXd pdfATasub = stdnormalPDF(normalizedasub);
            VectorXd pdfATbsub = stdnormalPDF(normalizedbsub);
            if(d > 1)
            {
                for(int j = 0; j < d; j++)
                {
                    normalizedLsubsub << normalizedLsub.block(0,0,j,j), normalizedLsub.block(0,1+j,j,d-1-j),
                     normalizedLsub.block(1+j,0,d-1-j,j), normalizedLsub.block(1+j,1+j,d-1-j,d-1-j);
                    normalizedasubsub << normalizedasub.segment(0,j), normalizedasub.segment(j+1,d-1-j);
                    normalizedbsubsub << normalizedbsub.segment(0,j), normalizedbsub.segment(j+1,d-1-j);
                    normalizedAsubsubCol << normalizedAsub.col(j).segment(0,j), normalizedAsub.col(j).segment(j+1,d-1-j);
                    VectorXd meanA = normalizedAsubsubCol * normalizedasub(j);
                    VectorXd meanB = normalizedAsubsubCol * normalizedbsub(j);
                    normalizedLsubsub.block(j,j,d-1-j,d-1-j) =
                     rank1update(normalizedLsubsub.block(j,j,d-1-j,d-1-j),normalizedLsub.col(j).segment(j+1,d-1-j));
                    normalizedLsubsub =
                     rank1downdate(normalizedLsubsub,normalizedAsubsubCol); 
                    mvn(RAND_POINT_NUM,normalizedLsubsub,normalizedasubsub-meanA,
                     normalizedbsubsub-meanA,psubsubLower,esubsub,SAMPLE_NUM);
                    mvn(RAND_POINT_NUM,normalizedLsubsub,normalizedasubsub-meanB,
                     normalizedbsubsub-meanB,psubsubUpper,esubsub,SAMPLE_NUM);
                    y(i+j) = pdfATasub(j) * psubsubLower - pdfATbsub(j) * psubsubUpper;   
                }
                psub = psub > ALMOSTONE? ALMOSTONE : psub;
                psub = psub < ALMOSTZERO? ALMOSTZERO : psub;
                y.segment(i,d) = ((normalizedAsub * y.segment(i,d) / psub).array() * stdevsub).matrix();
            }else
            {
                double cdfATasub, cdfATbsub;
                vdCdfNorm(1,normalizedasub.data(),&cdfATasub);
                vdCdfNorm(1,normalizedbsub.data(),&cdfATbsub);
                y(i) = (pdfATasub(0) - pdfATbsub(0)) / (cdfATbsub - cdfATasub) * stdevsub(0);
            }
        }
    }

    // If N is not a multiple of d
    if(N % d != 0)
    {
        int r = N % d;
        int i1 = N - r;
        double psub,esub,psubsubLower,psubsubUpper,esubsub;
        VectorXd g(r),normalizedasubsub(r-1),normalizedbsubsub(r-1),normalizedAsubsubCol(r-1);
        MatrixXd normalizedLsub(r,r), normalizedLsubsub(r-1,r-1);
        ArrayXd stdevsub(r);
        g = VectorXd::Zero(r);
        for(int j = 0; j < i1; j = j + d)
            g += L.block(i1,j,r,d) * L.block(j,j,d,d).inverse() * y.segment(j,d);
        normalizedLsub = L.block(i1,i1,r,r);
        MatrixXd normalizedAsub = normalizedLsub * normalizedLsub.transpose();
        stdevsub = normalizedAsub.diagonal().array().sqrt();
        VectorXd normalizedasub = ((a.segment(i1,r) - g).array() / stdevsub).matrix();
        VectorXd normalizedbsub = ((b.segment(i1,r) - g).array() / stdevsub).matrix();

        for(int j = 0; j < r; j++)
            for(int k = 0; k < r; k++)
            {
                normalizedLsub(j,k) /= stdevsub(j);
                normalizedAsub(j,k) /= (stdevsub(j) * stdevsub(k));
            }
        mvn(RAND_POINT_NUM,normalizedLsub,normalizedasub,normalizedbsub,psub,esub,SAMPLE_NUM);
        p = p * psub;


        // Calculate expectation
        VectorXd pdfATasub = stdnormalPDF(normalizedasub);
        VectorXd pdfATbsub = stdnormalPDF(normalizedbsub);
        if(r > 1)
        {
            for(int j = 0; j < r; j++)
            {
                normalizedLsubsub << normalizedLsub.block(0,0,j,j), normalizedLsub.block(0,1+j,j,r-1-j),
                 normalizedLsub.block(1+j,0,r-1-j,j), normalizedLsub.block(1+j,1+j,r-1-j,r-1-j);
                normalizedasubsub << normalizedasub.segment(0,j), normalizedasub.segment(j+1,r-1-j);
                normalizedbsubsub << normalizedbsub.segment(0,j), normalizedbsub.segment(j+1,r-1-j);
                normalizedAsubsubCol << normalizedAsub.col(j).segment(0,j), normalizedAsub.col(j).segment(j+1,r-1-j);
                VectorXd meanA = normalizedAsubsubCol * normalizedasub(j);
                VectorXd meanB = normalizedAsubsubCol * normalizedbsub(j);
                normalizedLsubsub.block(j,j,r-1-j,r-1-j) =
                 rank1update(normalizedLsubsub.block(j,j,r-1-j,r-1-j),normalizedLsub.col(j).segment(j+1,r-1-j));
                normalizedLsubsub =
                 rank1downdate(normalizedLsubsub,normalizedAsubsubCol);
                mvn(RAND_POINT_NUM,normalizedLsubsub,normalizedasubsub-meanA,
                 normalizedbsubsub-meanA,psubsubLower,esubsub,SAMPLE_NUM);
                mvn(RAND_POINT_NUM,normalizedLsubsub,normalizedasubsub-meanB,
                 normalizedbsubsub-meanB,psubsubUpper,esubsub,SAMPLE_NUM);
                y(i1+j) = pdfATasub(j) * psubsubLower - pdfATbsub(j) * psubsubUpper;
            }
            y.segment(i1,r) = ((normalizedAsub * y.segment(i1,r) / psub).array() * stdevsub).matrix();
        }else
        {
            double cdfATasub, cdfATbsub;
            vdCdfNorm(1,normalizedasub.data(),&cdfATasub);
            vdCdfNorm(1,normalizedbsub.data(),&cdfATbsub);
            y(i1) = (pdfATasub(0) - pdfATbsub(0)) / (cdfATbsub - cdfATasub) * stdevsub(0);
        }
    }
}









