# MKLROOT should be defined in the environment
Include = -I../eigen/  -I"${MKLROOT}/include"
Lib = ${MKLROOT}/lib/libmkl_intel_lp64.a ${MKLROOT}/lib/libmkl_intel_thread.a ${MKLROOT}/lib/libmkl_core.a -liomp5 -lpthread -lm -ldl
CC = g++
CCFlags = -m64 -std=c++11 -O3 


main: main.o hchol.o qmc.o conditioning.o reorder.o hcmvn.o commondcl.o 
	$(CC) main.o hchol.o hcmvn.o qmc.o conditioning.o reorder.o commondcl.o $(Lib) -o main


%.o: %.cpp
	$(CC) $(Include) $(CCFlags) -c $< -o $@


