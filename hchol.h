#ifndef HCHOL_H
#define HCHOL_H
#include "commondcl.h"

int hchol (Eigen::MatrixXd& A, int m, double tol, 
	        std::vector<Eigen::MatrixXd> & B, std::vector<treeNode> & UV);

void hstats(const Eigen::MatrixXd& L, const std::vector<Eigen::MatrixXd> & B, const std::vector<treeNode> & UV);

#endif
