#include <iostream>
#include <vector>
#include "Eigen/Dense"
#include "mkl.h"
#include "hcmvn.h"
#include "qmc.h"
#include "conditioning.h"
#include "commondcl.h"
#include "hchol.h"


using namespace std;
using namespace Eigen;


/*
 * The two-level hierarchical conditioning method
 *   Reordering is not included. In other words, reordering should be done
 *     prior to calling this function
 *
 * */
// [[Rcpp::export]]
double hccmvn(Eigen::MatrixXd covM, Eigen::VectorXd a, Eigen::VectorXd b, 
        int m, int d, double tol)
{
    int N = a.size();
    if(N % m != 0)
    {
        cout << "Block size should be a factor of the problem size\n" << endl;
        exit(1);
    }
    vector<MatrixXd> B;
    vector<treeNode> UV;
    hchol(covM, m, tol, B, UV);
    int bnum = B.size();
    double psub, p = 1.0;
    VectorXd y = VectorXd::Zero(N);
    VectorXd ysub;
    int crtrow = 0;
    for(int i = 0; i < bnum; i++)
    {
        int bsz = B[i].rows();
        // Calculation prob
        if(i > 0)
        {
            VectorXd g = UV[i-1].U * UV[i-1].V.transpose() * y.segment(UV[i-1].j1,UV[i-1].width);
            a.segment(UV[i-1].i1,UV[i-1].height) = a.segment(UV[i-1].i1,UV[i-1].height) - g;
            b.segment(UV[i-1].i1,UV[i-1].height) = b.segment(UV[i-1].i1,UV[i-1].height) - g;
        }
        kdConditioning(B[i], a.segment(crtrow, bsz), b.segment(crtrow, bsz),
                d, psub, ysub);
        p = p * psub;
        // y.segment(crtrow,bsz) = B[i].inverse() * ysub;
        cblas_dtrsv(CblasColMajor, CblasLower, CblasNoTrans, CblasNonUnit, 
                bsz, B[i].data(), bsz, ysub.data(), 1);
        crtrow += bsz;
    }
    return p;
}








