#include <iostream>
#include <random> 
#include <vector>
#include "mkl.h"
#include "Eigen/Dense"
#include "hchol.h"   

using namespace std; 
using namespace Eigen; 

vector<int> inorder(int i, int nlev);
vector<treeNode> buildTree(int n, int nlev);
vector<treeNode> permuteTree(const vector<treeNode> & UV,  const vector<int> & order);
void uncompress(const vector<MatrixXd> & B, const vector<treeNode> & UV, MatrixXd & A);
void lr (const MatrixXd & A, double tol, MatrixXd & U, MatrixXd & V, int & rank);

MatrixXd gaussianMat(int m, int n) {
	MatrixXd A(m, n); 

	std::mt19937 rng(rand());            // random-number engine is Mersenne Twister
	std::normal_distribution<double>  normal;

	for (int i = 0; i < m; i++)
		for (int j = 0; j < n; j++)
			A(i,j) = normal(rng); 
	return A; 
}


int qr(MatrixXd& A, MatrixXd & Q, MatrixXd & R)
{
	int m = A.rows(); 
	int n = A.cols(); 
	double tau[m]; 
	R = MatrixXd::Zero(n, n);
	Q = MatrixXd(m, n); 

	LAPACKE_dgeqrf(LAPACK_COL_MAJOR, m, n, A.data(), m, tau);
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			R(i, j) = j < i ? 0 : A(i, j); 

	LAPACKE_dorgqr(LAPACK_COL_MAJOR, m, n, n, A.data(), m, tau);
	for (int j = 0; j < n; j++)
		for (int i = 0; i < m; i++)
			Q(i, j) = A(i, j); 

	return 0; 
}


int svd(MatrixXd& A, MatrixXd & U, MatrixXd & V, VectorXd & S) {
	int m = A.rows(); 
	int n = A.cols();

	U = MatrixXd(m, n); 
	V = MatrixXd(n, n); 
	S = VectorXd(n); 
	double superb[n-1]; 

	int info = LAPACKE_dgesvd(LAPACK_COL_MAJOR, 'S', 'S', m, n, A.data(), m, S.data(), 
		                      U.data(), m, V.data(), n, superb);
	V.transposeInPlace(); 
    return 0;
}


int cholesky(MatrixXd& A)
{
	int n = A.rows(); 
	LAPACKE_dpotrf(LAPACK_COL_MAJOR, 'L', n, A.data(), n); 
    return 0;
}



// Build a dense Cholesky decomposition and put it in Hierarchical matrix format
int hchol (MatrixXd& A, int m, double tol, vector<MatrixXd> & B, vector<treeNode> & UV)
{	
	int n = A.rows(); 
	int nb = n / m; 
	int nlev = log2(n/m);  // cout << nlev << std::endl << std::endl; 
	if (m > n) return 1; 

	cholesky(A);
	MatrixXd & L = A; 

	B.resize(nb); 
	for (int i = 0; i < nb; i++) 
	{
		B[i] = L.block(i*m, i*m, m, m); 
		for (int j = 0; j < m; j++)
			for (int k = j + 1; k < m; k++)
				B[i](j,k) = 0.0;
	}
		

	if (nlev > 0) { 
		vector<int> order = inorder(0, nlev); 
		UV = buildTree(n, nlev);    //
		UV = permuteTree(UV, order); 
	}

	int c = 0; 
	for (auto ti = UV.begin(); ti != UV.end(); ti++) 
		lr(L.block(ti->i1, ti->j1, ti->height, ti->width), tol, ti->U, ti->V, ti->rank); 
	
	return 0;
}


void lr (const MatrixXd & A, double tol, MatrixXd & U, MatrixXd & V, int & rank)
{
	int n = A.rows(); 
	int kmax = n; // 16 + floor(3*sqrt((double) n));    // some kmax. better heuristic needed here
	kmax = kmax < n ? kmax : n; 
	MatrixXd Omega = gaussianMat(n, kmax); 
	MatrixXd Q, R, Uf, Vf; 
	VectorXd s; 

	MatrixXd AO = A * Omega; 
	qr(AO, Q, R); 

	AO = A.transpose() * Q; 
	svd(AO, Uf, Vf, s); 

	int k = 1;     // min rank is 1
	while ( (k < n) && (k < kmax) && (s[k] > tol) )
		k++; 
	rank = k; 
	if (k>1 && k==kmax) cout << "Warning: max rank reached " << kmax << endl; 

	VectorXd s2 = s.segment(0, k).cwiseSqrt();
	MatrixXd U2(n, k), V2(kmax, k); 
	for (int i = 0; i < n; i++)
		for (int j = 0; j < k; j++)
	 		U2(i, j) = Uf(i, j) * s2(j); 
	for (int i = 0; i < kmax; i++)
		for (int j = 0; j < k; j++)
	 		V2(i, j) = Vf(i, j) * s2(j);
	U = Q*V2; 
	V = U2; 
}


// void lr (const MatrixXd & A, double tol, MatrixXd & U, MatrixXd & V, int & rank)
// {
// 	int n = A.rows(); 
// 	int kmax = 16 + floor(4*sqrt((double) n));    // some kmax. better heuristic needed here
// 	double tol_svd = tol / 10 / 3;   // compute rsvd to tighter accuracy   sqrt(n) / sqrt(kmax); 
// 	MatrixXd Uf, Vf; 
// 	VectorXd s; 

// 	MatrixXd block(A);  
// 	int ier = rsvd(block, tol_svd, Uf, Vf, s, kmax); 
// 	kmax = s.size(); 
// 	int k = 1;     // min rank is 1
// 	while ( (k < n) && (k < kmax) && (s[k] > tol) )
// 		k++; 
// 	rank = k; 

// 	VectorXd s2 = s.segment(0, k).cwiseSqrt();
// 	U = Uf.block(0, 0, n, k); 
// 	V = Vf.block(0, 0, n, k);  		
// 	for (int i = 0; i < n; i++)
// 		for (int j = 0; j < k; j++) {
// 	 		U(i, j) *= s2(j);
// 	 		V(i, j) *= s2(j); 
// 		}

// 	if (k>1 && k==kmax) cout << "Warning: max rank reached " << kmax << endl; 
// }



// generates the explicit matrix---lower triangular part only
void uncompress(const vector<MatrixXd> & B, const vector<treeNode> & UV, MatrixXd & A) 
{
	int nb = B.size(); 
	int m = B[0].rows();  
	A = MatrixXd::Zero(nb*m, nb*m);
	for (int i = 0; i < nb; i++)
		A.block(i*m, i*m, m, m) = B[i]; 
	for (auto it = UV.begin(); it != UV.end(); it++)
		A.block(it->i1, it->j1, it->height, it->width) = it->U * it->V.transpose(); 
}

vector<treeNode> permuteTree(const vector<treeNode> & UV,  const vector<int> & order)
{
	vector<treeNode> UVnew(UV.size()); 
	for (int i = 0; i < UV.size(); i++) {
		UVnew[i].i1 = UV[order[i]].i1;
		UVnew[i].j1 = UV[order[i]].j1;
		UVnew[i].width = UV[order[i]].width;
		UVnew[i].height = UV[order[i]].height;
		UVnew[i].q = UV[order[i]].q;		
	}
	return UVnew; 
}

vector<treeNode> buildTree(int n, int nlev)
{	
	vector<treeNode> tree( (1<<nlev)-1 ); 
	for (int q = 0; q < nlev; q++) {
		int bsz = n / (1<<(q+1));   // 2^(q+1), size of block at level q
		for (int i = 0; i < (1<<q) ; i++) {
			int zi = (1<<q)-1 + i;      // index of block in binary tree
			// int ti = inorder[zi];       // its``inorder'' index
			// cout << zi << ' ' << ti << endl; 
			tree[zi].i1 = (2*i+1)*bsz;  
			tree[zi].j1 = 2*i*bsz;
			tree[zi].width = bsz;
			tree[zi].height = bsz;
			tree[zi].q = q+1;
		}
	}
	return tree; 
}

vector<int> inorder(int i, int nlev)
{	
	int n = (1<<(nlev-1)) - 1; // number of nodes before leaf level
	if (i > n-1 ) {   // i is a leaf
		vector<int> order(1); 
		order[0] = i; 
		return order;
	} else {
		int left = 2*i+1;      // left child
		int right = 2*i+2;   // right child
		vector<int> vleft = inorder(left, nlev);
		vector<int> parent(1); 
		parent[0] = i; 
		vector<int> vright = inorder(right, nlev);
		vector<int> order;
		order.reserve(vleft.size() + 1 + vright.size());
		order.insert( order.end(), vleft.begin(), vleft.end() );
		order.insert( order.end(), parent.begin(), parent.end() );
		order.insert( order.end(), vright.begin(), vright.end() );
		return order; 
	}
}

void hstats(const MatrixXd& L, const vector<MatrixXd> & B, const vector<treeNode> & UV)
{
	// displayTree(UV); 
	MatrixXd Lrecon; 
	uncompress(B, UV, Lrecon);
	Lrecon = Lrecon - L;
	Lrecon = Lrecon.triangularView<Lower>();
	cout <<  "Error (||L-LH||/||L||): " << Lrecon.norm() / L.norm() << endl; 

	int nb = B.size(); 
	int m = B[0].rows(); 
	int kmin = m*nb, kmax = 0, kave = 0; 
	int memory = 0;
	double mb; 

	for (auto it = UV.begin(); it != UV.end(); it++) {
		memory += it->width * it->rank; 
		kave += it->rank; 
		if (kmin > it->rank) 
			kmin = it->rank; 
		if (kmax < it->rank)
			kmax = it->rank; 
	}
	kave = kave / UV.size(); 
	mb = (memory+nb*m*m) * 8.0 / 1024.0 / 1024.0; 

	cout << "kmin: " << kmin << ", kave: " << kave << ", kmax: " << kmax 
	     << ", LR doubles: " << memory << ", Total MB: " <<  mb << endl; 
}
