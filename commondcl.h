#ifndef COMMONDCL_H
#define COMMONDCL_H
#include <vector>
#include "Eigen/Dense"
struct treeNode
{
    Eigen::MatrixXd U,V;
    int i1,j1,width,height,q,rank;
};
Eigen::MatrixXd rank1update(Eigen::MatrixXd L, Eigen::VectorXd x);
Eigen::MatrixXd rank1downdate(Eigen::MatrixXd L, Eigen::VectorXd x);
Eigen::VectorXd stdnormalPDF(const Eigen::VectorXd &v);
void reorderLimits(Eigen::VectorXd &oriLimits, std::vector<int>ind);
Eigen::MatrixXd denseCholfac(const std::vector<Eigen::MatrixXd> &B, const std::vector<treeNode> &UV);
Eigen::MatrixXd denseCovmat(const std::vector<Eigen::MatrixXd> &B, const std::vector<treeNode> &UV);
#endif // COMMONDCL_H
