#ifndef CONDITIONING_H
#define CONDITIONING_H
#include "Eigen/Dense"


void kdConditioning(const Eigen::MatrixXd &L,
    const Eigen::VectorXd &a, const Eigen::VectorXd &b, int d, double &p, Eigen::VectorXd &y);


#endif
