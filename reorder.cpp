#include <cmath>
#include <iostream>
#include <algorithm>
#include <numeric>
#include "reorder.h"
#include "Eigen/Dense"
#include "mkl.h"

using namespace std;
using namespace Eigen;


Eigen::VectorXd getSub(const Eigen::VectorXd &total, const std::vector<int> &ind)
{
    int sz = ind.size();
    Eigen::VectorXd sub(sz);
    for(int i = 0; i < sz; i++)
        sub(i) = total(ind[i]);
    return sub;
}
Eigen::MatrixXd getSub(const Eigen::MatrixXd &total, const std::vector<int> &ind)
{
    int sz = ind.size();
    Eigen::MatrixXd sub(sz,sz);
    for(int i = 0; i < sz; i++)
        for(int j = 0; j < sz; j++)
            sub(i,j) = total(ind[i],ind[j]);
    return sub;
}

/*
 * Iterative block reordering
 *   Return the order mapping, new -> old
 * */
// [[Rcpp::export]]
vector<int> recurUniandBlkcmb(const Eigen::MatrixXd &covM,
 const Eigen::VectorXd &a, const Eigen::VectorXd &b, int bsz_)
{
    int N = a.size();
    if(N % bsz_ != 0)
    {
        cout << "Block size should be a factor of the problem size\n" << endl;
        exit(1);
    }
    int bnum = N / bsz_;
    VectorXd probs(bnum);
    int i1 = 0;
    vector<int> ind(a.size());
    iota(ind.begin(), ind.end(), 0);
    for(int i = 0; i < bnum; i++)
    {
        MatrixXd A = covM.block(i1, i1, bsz_, bsz_);
        vector<int> subInd(ind.begin()+i1,ind.begin()+i1+bsz_);
        VectorXd a_ = getSub(a,subInd);
        VectorXd b_ = getSub(b,subInd);

        MatrixXf::Index minCoefPos;
        int tempInt;
        double tempDbl,tempLowerb,tempUpperb;
        double prob = 1.0;
        VectorXd y = VectorXd::Zero(bsz_);
        VectorXd tempVec(bsz_),tempVecRow(bsz_);
        for(int j = 0; j < bsz_; j++)
        {
            VectorXd s(bsz_-j),t(bsz_-j);
            if(j > 0)
            {
                y(j-1) = ((exp(-tempLowerb * tempLowerb / 2.0) -
                 exp(-tempUpperb * tempUpperb / 2.0)) / sqrt(2.0*M_PI)) / tempDbl;
                s = A.block(j,0,bsz_-j,j) * y.segment(0,j);
                t = A.block(j,0,bsz_-j,j).cwiseAbs2().rowwise().sum();
            }else
            {
                s = VectorXd::Zero(bsz_-j);
                t = VectorXd::Zero(bsz_-j);
            }
            VectorXd lowerBounds = ((a_.segment(j,bsz_-j) - s).array() /
             sqrt((A.diagonal().segment(j,bsz_-j) - t).array())).matrix();
            VectorXd upperBounds = ((b_.segment(j,bsz_-j) - s).array() /
             sqrt((A.diagonal().segment(j,bsz_-j) - t).array())).matrix();
            VectorXd cdfATlowerb(bsz_-j);
            VectorXd cdfATupperb(bsz_-j);
            vdCdfNorm(bsz_-j,lowerBounds.data(),cdfATlowerb.data());
            vdCdfNorm(bsz_-j,upperBounds.data(),cdfATupperb.data());
            VectorXd probVec = cdfATupperb - cdfATlowerb;
            probVec.minCoeff(&minCoefPos);
            minCoefPos += j;
            tempInt = subInd[j];
            subInd[j] = subInd[minCoefPos];
            subInd[minCoefPos] = tempInt;
            tempVecRow = A.row(j);
            A.row(j) = A.row(minCoefPos);
            A.row(minCoefPos) = tempVecRow;
            tempVec = A.col(j);
            A.col(j) = A.col(minCoefPos);
            A.col(minCoefPos) = tempVec;
            tempDbl = a_(j);
            a_(j) = a_(minCoefPos);
            a_(minCoefPos) = tempDbl;
            tempDbl = b_(j);
            b_(j) = b_(minCoefPos);
            b_(minCoefPos) = tempDbl;

            if(j > 0)
            {
                tempDbl = sqrt(A(j,j) - A.row(j).segment(0,j).cwiseAbs2().sum());
                A.col(j).segment(j,bsz_-j) = (A.col(j).segment(j,bsz_-j) -
                 A.block(j,0,bsz_-j,j)*A.row(j).segment(0,j).transpose()) / tempDbl;
            }else
            {
                tempDbl = sqrt(A(j,j));
                A.col(j).segment(j,bsz_-j) = A.col(j).segment(j,bsz_-j) / tempDbl;
            }

            if(j > 0)
            {
                tempLowerb = (a_(j) - A.row(j).segment(0,j) * y.segment(0,j)) / A(j,j);
                tempUpperb = (b_(j) - A.row(j).segment(0,j) * y.segment(0,j)) / A(j,j);
            }else
            {
                tempLowerb = a_(j) / A(j,j);
                tempUpperb = b_(j) / A(j,j);
            }
            vdCdfNorm(1,&tempLowerb,cdfATlowerb.data());
            vdCdfNorm(1,&tempUpperb,cdfATupperb.data());
            tempDbl = cdfATupperb(0) - cdfATlowerb(0);
            prob *= tempDbl;
        }
        copy_n(subInd.begin(),bsz_,ind.begin()+i1);
        probs(i) = prob;
        i1 += bsz_;
    }

    vector<int> blkInd(bnum);
    iota(blkInd.begin(),blkInd.end(),0);
    sort(blkInd.begin(),blkInd.end(), [&probs](int i1, int i2) {return probs[i1] < probs[i2];});
    vector<int> indCopy = ind;
    i1 = 0;
    for(int i = 0; i < bnum; i++)
    {
        int offset = 0;
        for(int j = 0; j < blkInd[i]; j++)
            offset += bsz_;
        copy_n(indCopy.begin()+offset,bsz_,ind.begin()+i1);
        i1 += bsz_;
    }
    return ind;
}


