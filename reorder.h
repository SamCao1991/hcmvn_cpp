#ifndef REORDER_H
#define REORDER_H
#include <vector>
#include "Eigen/Dense"
std::vector<int> recurUniandBlkcmb(const Eigen::MatrixXd &covM, const Eigen::VectorXd &a, 
        const Eigen::VectorXd &b, int bsz_);
#endif
