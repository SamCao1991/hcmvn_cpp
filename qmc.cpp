#include <vector>
#include <cmath>
#include <iostream>
#include <numeric>

#include "qmc.h"
#include "mkl.h"

#define ALMOSTONE 0.9999999999
#define ALMOSTZERO 0.0000000001

using namespace std;
using namespace Eigen;

vector<int> primes(int n);
void mean_std (const vector<double> & v, double & mean, double & stdev);
int mvndns(int n,  int N, const MatrixXd& L, const MatrixXd & x,
	       const MatrixXd & a, const MatrixXd & b, RowVectorXd & p, MatrixXd & y);

int mvn(int N, const MatrixXd& L, const VectorXd & a1, const VectorXd & b1,
	double & v, double & e, int ns)
{
	int n = L.rows();
	vector<double> values(ns);    // values produced by the ns samples, each with N randomized qmc points
 	MatrixXd x(n, N);
	ArrayXXd a = a1.replicate(1, N);
 	ArrayXXd b = b1.replicate(1, N);
 	RowVectorXd p;
 	MatrixXd y;

	vector<int> prime = primes(5*n*log((double)n+1)/4);
	VectorXd q(n); for (int i=0; i < n; i++) q(i) = sqrt((double) prime[i]);
	RowVectorXd one2N(N);  for (int i=0; i< N; i++)  one2N(i) = (double) (i+1);
	MatrixXd qN; qN.noalias() = q * one2N; // qN = qN.unaryExpr(&mod1);

 	for (int i = 0; i < ns; i++) {
 		MatrixXd xr = MatrixXd::Random(n,1);
 		xr = xr.unaryExpr( [](double x) {return (x+1)*0.5;} );
 		for (int j = 0; j < N; j++)
 			x.col(j) = qN.col(j) + xr;
 		x = x.unaryExpr( [](double x) {return  abs(2*(x-(int(x)))-1); });
 		mvndns(n, N, L, x, a, b, p, y);
 		values[i] = p.mean();
 	}
 	mean_std(values, v, e);
 	e = 2 * e / sqrt( (double) ns);
 	// cout << "mean: " << v << "   stdev: " << e << endl;

 	return 0;
}

int hmvn(int N, const vector<MatrixXd> & B, const vector<treeNode> & UV, const VectorXd & a1,
	     const VectorXd & b1, double & v, double & e, int ns)
{
	int nb = B.size();
	int m = B[0].rows();
	int n = nb * m;
	vector<double> values(ns);    // values produced by the ns samples, each with N randomized qmc points
	vector<int> prime = primes(5*n*log((double)n+1)/4);
	VectorXd q(n); for (int i=0; i < n; i++) q(i) = sqrt((double) prime[i]);
	RowVectorXd one2N(N);  for (int i=0; i< N; i++)  one2N(i) = (double) (i+1);
 	MatrixXd vp(nb, N);
 	MatrixXd y(nb*m, N);
 	RowVectorXd pr;
 	MatrixXd yr, delta;
 	MatrixXd x(m, N);

 	for (int i = 0; i < ns; i++) {
  		MatrixXd xr = MatrixXd::Random(m,nb);
 		xr = xr.unaryExpr( [](double x) {return (x+1)*0.5;} );
		MatrixXd a = a1.replicate(1, N);
 		MatrixXd b = b1.replicate(1, N);
 		for (int r = 0; r < nb; r++) {
 			int r1 = r*m;
 			if (r > 0) {
 				int i1 = UV[r-1].i1; int j1 = UV[r-1].j1; int bsz = UV[r-1].width;
 				delta.noalias() = UV[r-1].U * ( UV[r-1].V.transpose() * y.block(j1,0,bsz,N) );
 				a.block(i1, 0, bsz, N) -= delta;
 				b.block(i1, 0, bsz, N) -= delta;
 			}
 			MatrixXd qN = q.segment(r1, m) * one2N;
			for (int j = 0; j < N; j++)
 				x.col(j) = qN.col(j) + xr.col(r);
 			x = x.unaryExpr( [](double x) {return  abs(2*(x-(int(x)))-1); });
 			mvndns(m, N, B[r], x, a.block(r1,0,m,N), b.block(r1,0,m,N), pr, yr);
 			vp.row(r) = pr;
 			y.block(r1,0,m,N) = yr;
 		}
 		RowVectorXd p = vp.colwise().prod();
 		values[i] = p.mean();
 	}
 	mean_std(values, v, e);
 	e = 2 * e / sqrt( (double) ns);
 	// cout << "mean: " << v << "   stdev: " << e << endl;

	return 0;
}

vector<int> primes(int n)
{
	vector<int> v;
	v.push_back(2);
	for (int i = 3; i < n; i++) {
		bool prime = true;
		for (int j=2; j*j <= i; j++)
			if (i % j == 0) {
				prime = false;
				break;
			}
		if (prime)
			v.push_back(i);
	}
	return v;
}

void mean_std (const vector<double> & v, double & mean, double & stdev)
{
	double sum = std::accumulate(v.begin(), v.end(), 0.0);
	mean = sum / v.size();

	std::vector<double> diff(v.size());
	std::transform(v.begin(), v.end(), diff.begin(), [mean](double x) { return x - mean; });
	double sq_sum = std::inner_product(diff.begin(), diff.end(), diff.begin(), 0.0);
	stdev = std::sqrt(sq_sum / v.size());
}

int mvndns(int n,  int N, const MatrixXd& L, const MatrixXd & x,
	       const MatrixXd & a, const MatrixXd & b, RowVectorXd & p, MatrixXd & y)
{
	RowVectorXd c(N), d(N), buf(N), dc(N);

	y = MatrixXd::Zero(n, N);
	p = ArrayXd::Ones(N);
	RowVectorXd s = RowVectorXd::Zero(N);

	for (int i = 0; i < n; i++) {
		if (i > 0) {
			c += x.row(i-1).cwiseProduct(dc);
			c = c.unaryExpr( [](double x) {return x > ALMOSTONE ? ALMOSTONE : x;} );
			c = c.unaryExpr( [](double x) {return x < ALMOSTZERO ? ALMOSTZERO : x;} );
			vdCdfNormInv(N, c.data(), buf.data());
			y.row(i-1) = buf;
			s.noalias() = L.row(i).segment(0,i) * y.block(0, 0, i, N);
		}
		double ct = L(i,i);
		RowVectorXd ai = a.row(i) - s;  ai /= ct;
		RowVectorXd bi = b.row(i) - s;  bi /= ct;
		vdCdfNorm(N, ai.data(), c.data());
		vdCdfNorm(N, bi.data(), d.data());
		dc = d - c;
		p =  p.array() * dc.array();
	}
	c += x.row(n-1).cwiseProduct(dc);
	c = c.unaryExpr( [](double x) {return x > ALMOSTONE ? ALMOSTONE : x;} );
	vdCdfNormInv(N, c.data(), buf.data());
	y.row(n-1) = buf;

	return 0;
}
