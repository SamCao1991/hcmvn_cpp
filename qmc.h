#ifndef QMC_H
#define QMC_H
#include <vector>
#include "Eigen/Dense"
#include "commondcl.h"
int mvn(int N, const Eigen::MatrixXd& L, const Eigen::VectorXd & a1, 
        const Eigen::VectorXd & b1, double & v, double & e, int ns = 10);
int hmvn(int N, const std::vector<Eigen::MatrixXd> & B, 
        const std::vector<treeNode> & UV, const Eigen::VectorXd & a1,
	    const Eigen::VectorXd & b1, double & v, double & e, int ns);
#endif
