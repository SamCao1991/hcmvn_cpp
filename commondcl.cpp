#include <cmath>
#include "commondcl.h"
using namespace std;
using namespace Eigen;


MatrixXd rank1update(MatrixXd L, VectorXd x)
{
    int n = x.size();
    double r,c,s;
    for(int i = 0; i < n; i++)
    {
        r = sqrt(L(i,i) * L(i,i) + x(i)*x(i));
        c = r / L(i,i);
        s = x(i) / L(i,i);
        L(i,i) = r;
        L.block(i+1,i,n-1-i,1) = (L.block(i+1,i,n-1-i,1) + s * x.segment(i+1,n-1-i)) / c;
        x.segment(i+1,n-1-i) = c * x.segment(i+1,n-1-i) - s * L.block(i+1,i,n-1-i,1);
    }
    return L;
}

MatrixXd rank1downdate(MatrixXd L, VectorXd x)
{
    int n = x.size();
    double r,c,s;
    for(int i = 0; i < n; i++)
    {
        r = sqrt(L(i,i) * L(i,i) - x(i)*x(i));
        c = r / L(i,i);
        s = x(i) / L(i,i);
        L(i,i) = r;
        L.block(i+1,i,n-1-i,1) = (L.block(i+1,i,n-1-i,1) - s * x.segment(i+1,n-1-i)) / c;
        x.segment(i+1,n-1-i) = c * x.segment(i+1,n-1-i) - s * L.block(i+1,i,n-1-i,1);
    }
    return L;
}

VectorXd stdnormalPDF(const VectorXd &v)
{
    return (exp(-v.array().pow(2.0)/2.0) / sqrt(2.0 * M_PI)).matrix();
}

void reorderLimits(Eigen::VectorXd &oriLimits, std::vector<int>ind)
{
    VectorXd temp = oriLimits;
    for(int i = 0; i < ind.size(); i++)
        oriLimits[i] = temp[ind[i]];
}

Eigen::MatrixXd denseCholfac(const std::vector<Eigen::MatrixXd> &B, const std::vector<treeNode> &UV)
{
    int nb = B.size();
    int N = 0;
    for(int i = 0; i < nb; i++)
        N += B[i].rows();
    MatrixXd denCholFactor = MatrixXd::Zero(N,N);
    int i1 = 0;
    for(int i = 0; i < nb; i++)
    {
        denCholFactor.block(i1,i1,B[i].rows(),B[i].rows()) = B[i];
        if(i > 0)
            denCholFactor.block(UV[i-1].i1,UV[i-1].j1,UV[i-1].height,UV[i-1].width) = UV[i-1].U * UV[i-1].V.transpose();
        i1 += B[i].rows();
    }
    return denCholFactor;
}

Eigen::MatrixXd denseCovmat(const std::vector<Eigen::MatrixXd> &B, const std::vector<treeNode> &UV)
{
    return(denseCholfac(B,UV) * denseCholfac(B,UV).transpose());
}


